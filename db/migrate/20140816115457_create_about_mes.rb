class CreateAboutMes < ActiveRecord::Migration
  def change
    create_table :about_mes do |t|
      t.string :name
      t.string :tag
      t.string :greeting
      t.text :mylef
      t.string :sign

      t.timestamps
    end
  end
end
